<html>

<head>
  <title>Mapa de las propiedades</title>
  <script
    src="https://maps.cercalia.com/maps/loader.js?key=926c675b5577b9d5e85bf62c9f4462f9993f22eba5fd248bbe42a8c3a423966b&v=5&lang=es&theme=1976d2"></script>
</head>

<body>

  <?php
  $ubicaciones = [];
  $url = 'http://procesos.apinmo.com/xml/v2/Ru7VkGYC/10143-web.xml ';
  $xml = file_get_contents($url);
  $data = simplexml_load_string($xml);

  foreach ($data->propiedad as $propiedad) {
    $ubicacion = (string) $propiedad->ciudad;
    array_push($ubicaciones, $ubicacion);

    $latitud = (float) $propiedad->latitud;
    $altitud = (float) $propiedad->altitud;


    $id = (string) $propiedad->id;
    $ref = (string) $propiedad->ref;
    $titulo1 = (string) $propiedad->titulo1;
    $foto1 = (string) $propiedad->foto1;
    $accion = (string) $propiedad->accion;
    $ciudad = (string) $propiedad->ciudad;
    $precioinmo = (string) $propiedad->precioinmo;
    $tipo_oferta = (string) $propiedad->tipo_ofer;
    $banyos = (string) $propiedad->banyos;
    $habitaciones = (string) $propiedad->habitaciones;
    $habdobles = (string) $propiedad->habdobles;
    $m_cons = (string) $propiedad->m_cons;
    $conservacion = (string) $propiedad->conservacion;
    $zona = (string) $propiedad->zona;




    $mapObject = array(
      'id' => $id,
      'latitud' => $latitud,
      'altitud' => $altitud,
      'ref' => $ref,
      'titulo1' => $titulo1,
      'foto1' => $foto1,
      'accion' => $accion,
      'habdobles' => $habdobles,
      'ciudad' => $ciudad,
      'precioinmo' => $precioinmo,
      'tipo_oferta' => $tipo_oferta,
      'banyos' => $banyos,
      'habitaciones' => $habitaciones,
      'm_cons' => $m_cons,
      'conservacion' => $conservacion,
      'zona' => $zona,
    );

    $mapObjects[] = $mapObject;

  }

  $jsonMapObjects = json_encode($mapObjects);

  // Use $jsonMapObjects in your JavaScript code
  echo "<script>";
  echo "var mapObjects = $jsonMapObjects;";
  echo "</script>";
  ?>




  <div id="map" class="map"></div>

  <script>


    document.addEventListener("cercalia-ready", initMap);

    var map;

    function initMap() {
      map = new cercalia.Map({
        target: "map",
        controls: [],
      });

      var arrayMarkers = [];


      for (var i = 0; i < mapObjects.length; i++) {


        // Create a new variable for each mapObject
        var markerSantander = new cercalia.Marker({
          position: new cercalia.LonLat(mapObjects[ i ].altitud, mapObjects[ i ].latitud),
          popup: new cercalia.Popup({
            title: "Ref: " + mapObjects[ i ].ref + " " + mapObjects[ i ].accion,
            content: `<img src="${mapObjects[ i ].foto1}" alt="" style="width:300px;">` + mapObjects[ i ].conservacion + "<br> Zona: " + mapObjects[ i ].zona + "<br>" + mapObjects[ i ].habdobles + " hab." + mapObjects[ i ].banyos + " baños" + "<br> m.cons: " + mapObjects[ i ].m_cons + " m2" + "<br>Precio: " + mapObjects[ i ].precioinmo + " €"
              + "<br>" + `<a href="propiedad.php?id=${mapObjects[ i ].id}">Ver</a>`,
            minWidth: 200,
            maxWidth: 600,
            visible: false,
          }),
          infoMap: new cercalia.InfoMap({
            title: "Info",
            content: "<h1>Content....</h1><br/><b>hola</b>",
          }),
          onClick: function (marker) {
            marker.showInfoMap();
          },
        });







        arrayMarkers.push(markerSantander);
      }








      map.addMarkers(arrayMarkers);
    }
  </script>
</body>

</html>